#ifndef GENERATOR_DEFS_H
#define GENERATOR_DEFS_H

#include <fstream>
#include <iomanip>
#include <iostream>
#include <string>
#include <variant>
#include <vector>

using Tag = uint16_t;
using Length = uint16_t;

template<typename... Ts>
struct Overloaded : Ts... {
    using Ts::operator()...;
};

template<typename... Ts>
Overloaded(Ts...) -> Overloaded<Ts...>;

template<typename T>
struct Binary {
    Binary(const T &data)
        : _data(data)
    {}

    const char *data() const {
        return reinterpret_cast<const char *>(&_data);
    }

    constexpr std::size_t size() const {
        return sizeof(T);
    }

private:
    T _data;
};

template<typename T>
std::ostream &operator<<(std::ostream &out, const Binary<T> &binary) {
    out.write(binary.data(), binary.size());
    return out;
}

struct Item {
    Item(Tag tag)
        : _tag(tag)
    {}

    Item(Tag tag, std::string text)
        : _tag(tag)
        , _data(std::move(text))
    {}

    Item(Tag tag, int value)
        : _tag(tag)
        , _data(value)
    {}

    Item(Tag tag, double value)
        : _tag(tag)
        , _data(value)
    {}

    friend std::ostream &operator<<(std::ostream &out, const Item &item) {
        out << Binary(item._tag);
        std::visit(Overloaded {
            [&](const std::monostate &) { out << Binary<Length>(0); },
            [&](const std::string &text) { out << Binary<Length>(text.size()) << text; },
            [&](int data) { out << Binary<Length>(sizeof(data)) << Binary(data); },
            [&](double data) { out << Binary<Length>(sizeof(data)) << Binary(data); }
        }, item._data);
        return out;
    }

private:
    Tag _tag;
    std::variant<
        std::monostate,
        std::string,
        int,
        double
    > _data;
};

#endif