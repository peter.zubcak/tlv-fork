#ifndef TVL_H
#define TVL_H

#include <cstdint>
#include <optional>
#include <string>
#include <iterator>

// Declarations

struct tlv_par
{
    uint16_t tag;
    std::string_view value;
};

struct tlv_iterator
{
  using iterator_category = std::input_iterator_tag;
  using value_type = tlv_par;
  using difference_type = std::ptrdiff_t;
  using pointer = void;
  using reference = tlv_par &;

  tlv_iterator() {}
  tlv_iterator(std::string_view buffer) : buff(buffer) {
  }

  tlv_iterator &operator++() {
      buff.remove_prefix(sizeof(uint16_t));
      uint16_t length = *((uint16_t *)buff.substr(0, sizeof(uint16_t)).data());
      buff.remove_prefix(sizeof(uint16_t));
      buff.remove_prefix(length);
      return *this;
  }

  const tlv_par *operator*() {
      std::string_view pom_buff = buff;
      par.tag = *((uint16_t *)pom_buff.data());
      pom_buff.remove_prefix(sizeof(uint16_t));

      uint16_t length = *((uint16_t *)pom_buff.substr(0, sizeof(uint16_t)).data());
      pom_buff.remove_prefix(sizeof(uint16_t));
      par.value = pom_buff.substr(0, length);

      return &par;
  }

  friend inline bool operator==(const tlv_iterator &lhs, const tlv_iterator &rhs)
  {
      return lhs.buff.data() == rhs.buff.data();
  }


private:
  tlv_par par;
  std::string_view buff;
};

inline bool operator!=(const tlv_iterator &lhs, const tlv_iterator &rhs)
{
    return !(lhs == rhs);
}


class tlv_decoder
{
public:

  tlv_decoder(const std::string &b) : buff(b) { }

  tlv_iterator begin() {
      return tlv_iterator(buff);
  }

  tlv_iterator end() {
      std::string_view empty_buff(buff.data() + buff.length(), 0);
      return tlv_iterator(empty_buff);
  }

  const tlv_iterator begin() const {
      return tlv_iterator(buff);
  }

  const tlv_iterator end() const  {
      std::string_view empty_buff(buff.data() + buff.length(), 0);
      return tlv_iterator(empty_buff);
  }

  std::optional<tlv_par> get_tag(uint16_t tag) {
      for (const auto &it : *this) {
         if (it->tag == tag) {
             return *it;
         }
      }
      return std::nullopt;
  }

private:
  std::string_view buff;
};




#endif
