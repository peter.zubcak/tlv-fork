#include <fstream>
#include <iostream>
#include <string>
#include <map>
#include <functional>
#include <algorithm>

#include "tlv.h"

auto string_func = [](const tlv_par &par) -> void  {
    std::cout << "tag : " << par.tag << " value: " << par.value << std::endl;
};

auto int_func = [](const tlv_par &par) -> void  {
    std::cout << "tag : " << par.tag << " value: " << *(int*)par.value.data() << std::endl;
};

auto double_func = [](const tlv_par &par) -> void  {
    std::cout << "tag : " << par.tag << " value: " << *(double*)par.value.data() << std::endl;
};

auto void_func = [](const tlv_par &par) -> void  {
    std::cout << "tag : " << par.tag << " value: " << std::endl;
};

std::map<uint16_t, std::function<void(const tlv_par &par)>> map_interpreter = {
 {2, string_func},
 {18, string_func},
 {0, void_func},
 {188, int_func},
 {19, string_func},
 {20, string_func},
 {21, int_func}
};


int main(int argc, char **argv) {

    if (argc != 2) {
        std::cerr << "missing argument" << std::endl;
        return EXIT_FAILURE;
    }

    std::ifstream f(argv[1], f.in | f.binary);
    if (f.fail()) {
        std::cerr << "file " << argv[1] << " could not be opened" << std::endl;
        return EXIT_FAILURE;
    }

    std::string content(std::istreambuf_iterator<char>(f),{});// std::istreambuf_iterator<char>());
    tlv_decoder dec(content);

    std::for_each(dec.begin(), dec.end(), [](const tlv_par *it) {
        auto ij = map_interpreter.find(it->tag);
        if (ij != map_interpreter.end()) {
            ij->second(*it);
        }
    });

   for (const auto &it : dec) {
       auto ij = map_interpreter.find(it->tag);
       if (ij != map_interpreter.end()) {
           ij->second(*it);
       }
   }

//   std::cout << "moja hodnota: " << dec.get_tag(2)->value << std::endl;

    // ...

    return EXIT_SUCCESS;
}

